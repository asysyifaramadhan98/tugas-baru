﻿CREATE TABLE [dbo].[Pegawai] (
	[NIP] NVARCHAR (13)  NOT NULL, 
	[DivisiID] INT NOT NULL,
	[Nama]NVARCHAR (50)  NULL,
	[Alamat] NVARCHAR (150) NULL,
	[TanggalLahir] DATETIME NULL, 
	PRIMARY KEY CLUSTERED ([NIP] ASC)
	CONSTRAINT [FK_Pegawai_To_Divisi] FOREIGN KEY ([DivisiID]) REFERENCES 
[dbo].[Divisi] ([DivisiID])
)
